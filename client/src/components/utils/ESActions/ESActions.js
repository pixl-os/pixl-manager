import React from 'react';
import PropTypes from 'prop-types';
import { translate } from 'react-i18next';
import Panel from 'react-bootstrap/lib/Panel';
import PostActionButton from '../PostActionButton';

const renderStatusAction = (t, status, onSuccess, onError) => {
  if ('OK' === status) {
    return (
      <PostActionButton bsStyle="danger" action="shutdown-pf"
        onSuccess={onSuccess} onError={onError}>
        {t("Arrêter PF")}
      </PostActionButton>
    );
  }

  return (
    <PostActionButton bsStyle="success" action="start-pf"
      onSuccess={onSuccess} onError={onError}>
      {t("Démarrer PF")}
    </PostActionButton>
  );
};

const ESActions = ({ t, status, onSuccess, onError }) => (
  <Panel bsStyle="warning"
    header={<h3>{t("Redémarrage et arrêt de pegasus-frontend")}</h3>}>
    <PostActionButton bsStyle="warning" action="reboot-pf"
      onSuccess={onSuccess} onError={onError}>
      {t("Redémarer PF")}
    </PostActionButton>{' '}

    {renderStatusAction(t, status, onSuccess, onError)}
  </Panel>
);

ESActions.propTypes = {
  t: PropTypes.func.isRequired,
  status: PropTypes.oneOf(['OK', 'KO']).isRequired,
  onSuccess: PropTypes.func.isRequired,
  onError: PropTypes.func.isRequired,
};

export default translate()(ESActions);
