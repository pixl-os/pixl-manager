function translatableConfig(i18n) {
  return {
    recalbox: {
      // Audio
      /*audio: {
        devices: [
          { id: '', text: '–' },
          { id: 'auto', text: i18n.t('Automatique') },
          { id: 'hdmi', text: i18n.t('Prise HDMI') },
          { id: 'jack', text: i18n.t('Prise Jack') },
        ]
      },*/

      // Controllers
      controllers: {
        ps3drivers: [
          { id: 'official', text: i18n.t('Officiel') },
          { id: 'shanwan', text: i18n.t('Shanwan') },
          { id: 'bluez', text: i18n.t('Bluez 5') },
        ]
      },

      // Configuration
      configuration: {
        /*updatesTypes: [
          { id: '', text: '–' },
          { id: 'stable', text: i18n.t('Stable') },
          { id: 'beta', text: i18n.t('Bêta') },
          { id: 'unstable', text: i18n.t('Instable') },
        ],*/
      },

      // Systems
      systems: {
        ratio: [
          { id: '1/1', text: '1/1' },
          { id: '16/10', text: '16/10' },
          { id: '16/15', text: '16/15' },
          { id: '16/9', text: '16/9' },
          { id: '19/12', text: '19/12' },
          { id: '19/14', text: '19/14' },
          { id: '2/1', text: '2/1' },
          { id: '21/9', text: '21/9' },
          { id: '3/2', text: '3/2' },
          { id: '3/4', text: '3/4' },
          { id: '30/17', text: '30/17' },
          { id: '32/9', text: '32/9' },
          { id: '4/1', text: '4/1' },
          { id: '4/3', text: '4/3' },
          { id: '5/4', text: '5/4' },
          { id: '6/5', text: '6/5' },
          { id: '7/9', text: '7/9' },
          { id: '8/3', text: '8/3' },
          { id: '8/7', text: '8/7' },
          { id: '9/16', text: '9/16' },
          { id: 'auto', text: i18n.t('Automatique') },
          { id: 'config', text: i18n.t('Configuration RetroArch')},
          { id: 'coreprovided', text: i18n.t('Fourni par le core') },
          { id: 'none', text: i18n.t('Ne pas régler') },
          { id: 'squarepixel', text: i18n.t('Pixel carré') },
          { id: 'custom', text: i18n.t('Personnalisé') },
        ],

        shaderset: [
          { id: 'none', text: i18n.t('Aucun') },
          { id: 'retro', text: i18n.t('Retro') },
          { id: 'scanlines', text: i18n.t('Scanlines') },
        ],
      },
    },
  };
}

module.exports = translatableConfig;
