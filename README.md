# pixl-manager

A web interface to manage pixl configuration.

## Note
The API activation step is only needed if you want launch game through the web interface. Otherwise, it's not necessary.

## Installation from sources
1. Clone or download the repository: `git clone https://gitlab.com/pixl-os/pixl-manager`
#2. Compile the project from a computer (not pixl): `./compile.sh`
#3. Activate the API: https://gitlab.com/pixl-os/pixl-api/blob/1.1.x/documentation/activate-on-recalbox.md
#4. Launch the server (connect through SSH on the pixl): `cd /recalbox/share/manager && NODE_ENV=production PORT=3000 node dist/server.js`
2. Go on http://pixl/

## Installation from releases
#1. Download the release: https://gitlab.com/pixl-os/pixl-manager/releases/
#2. Extract the package on pixl: `\recalbox\share\manager` for example
#3. Activate the API: https://gitlab.com/pixl-os/pixl-api/blob/1.1.x/documentation/activate-on-recalbox.md
#4. Launch the server (connect through SSH on the pixl): `cd /recalbox/share/manager && NODE_ENV=production PORT=3000 node dist/server.js`
#5. Go on http://pixl:3000/

## Known issues
* Some changes are not directly applied; The pixl must be restarted.

## Contributing
* **Translations** are managed with [POEditor](https://poeditor.com/join/project/taFNFlZ840)
